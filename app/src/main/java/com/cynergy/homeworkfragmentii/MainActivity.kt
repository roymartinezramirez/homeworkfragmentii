package com.cynergy.homeworkfragmentii

import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.cynergy.homeworkfragmentii.Fragments.MapFragment
import com.cynergy.homeworkfragmentii.Fragments.ShoppingFragment
import com.cynergy.homeworkfragmentii.Fragments.StoreFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_map.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val FRAGMENT_SHOPPING = 0
    private val FRAGMENT_STORE = 1
    private val FRAGMENT_MAP = 2

    private val indicatorViews = mutableListOf<View>()
    private val titleViews = mutableListOf<TextView>()
    private var currentIndicator = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        indicatorViews.apply {
            add(ivShopping)
            add(ivStore)
            add(ivMap)
        }

        titleViews.apply {
            add(tvShopping)
            add(tvStore)
            add(tvMap)
        }
        titleViews.forEach {
            it.setOnClickListener(this)
        }

        selectFirst()
    }

    private fun selectFirst() {
        var bundle = Bundle()
        val fragmentId = FRAGMENT_SHOPPING
        updateUI(fragmentId)
        changeFragment(bundle,fragmentId)
    }

    override fun onClick(view: View?) {
        val bundle = Bundle()

        var fragmentId = when(view?.id){
            R.id.tvShopping -> FRAGMENT_SHOPPING
            R.id.tvStore -> FRAGMENT_STORE
            R.id.tvMap -> FRAGMENT_MAP
            else -> FRAGMENT_SHOPPING
        }

        updateUI(fragmentId)
        changeFragment(bundle,fragmentId)
    }

    private fun changeFragment(bundle: Bundle,fragmentId: Int) {
        val fragment =  factoryFragment(bundle,fragmentId)

        fragment?.let {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.frameContainer,it)
                addToBackStack(null)
                commit()
            }
        }
    }

    private fun factoryFragment(bundle: Bundle,fragmentId: Int): Fragment? {
        when(fragmentId){
            FRAGMENT_SHOPPING -> return ShoppingFragment()
            FRAGMENT_STORE -> return StoreFragment()
            FRAGMENT_MAP -> return MapFragment()
        }
        return null
    }

    private fun updateUI(fragmentId: Int) {
        if (currentIndicator >= 0){
            indicatorViews[currentIndicator].setBackgroundColor(Color.TRANSPARENT)
            titleViews[currentIndicator].inputType = Typeface.BOLD
        }
        indicatorViews[fragmentId].setBackgroundColor(Color.parseColor("#ffeb3b"))
        titleViews[fragmentId].inputType = Typeface.BOLD
        currentIndicator = fragmentId
    }

}